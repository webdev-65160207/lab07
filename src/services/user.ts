import type { User } from '@/types/User'
import http from './http'

function addUser(user: User) {
    return http.post('/users',user)
}

function updateUser(user: User) {
    return http.patch(`/user/${user.id}`,user)
}

function delUser(user: User) {
    return http.delete(`/user/${user.id}`)
}

function getUser(id: User) {
    return http.delete(`/user/${id}`)
}

function getUsers() {
    return http.get('/users')
}

export default { addUser, updateUser, delUser, getUser, getUsers }
